export class Settings {

  /**
   * This property is modified from ./environments folder <br>
   * So it allows to define a different backend URL based on target env
   *
   * @static
   * @type {string}
   * @memberOf Settings
   */
  static backendUrl: string;
}
