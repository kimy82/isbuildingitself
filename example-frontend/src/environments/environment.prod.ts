import { Settings } from '../settings';

Settings.backendUrl = '/backend/';

export const environment = {
  production: true
};
