/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';

import { AppComponent } from '../app/app.component';

import constants from './utils/constants';
import ComponentWrapper from './utils/ComponentWrapper';

describe('AppComponent', () => {

  let componentUnderTest: ComponentWrapper<AppComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule(constants.ALL_PRE_CONFIGURATION).compileComponents();
    componentUnderTest = new ComponentWrapper(AppComponent, true);
  });

  /**
   * Testing that starts the app with all its components
   */
  it('should create the app', async(() => {
    expect(componentUnderTest.getComponentInstance()).toBeTruthy();
  }));
});
