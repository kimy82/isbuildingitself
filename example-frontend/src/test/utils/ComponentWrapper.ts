import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { Component, Input, OnInit, Type } from '@angular/core';

/**
 * Holds a real component and provides Test utilities.
 */
export class ComponentWrapper<T> {

    private component: Type<T>;
    private fixture: ComponentFixture<T>;
    private instance: T;

    /**
     *
     * @param {boolean} detectChanges
     *       If true will begin ng lifecycle, so invokes ngOnInit <br>
     *       Use false when ngOn** uses @Input variables, which MUST in that case be manually provided
     */
    constructor(component: Type<T>, detectChanges: boolean) {
        this.component = component;
        this.fixture = this.buildFixture(component);
        this.instance = this.fixture.componentInstance;

        if (detectChanges) {
            this.fixture.detectChanges();
        }
    }

    public getFixture(): ComponentFixture<T> {
        return this.fixture;
    }

    public getComponentInstance(): T {
        return this.instance;
    }

    private buildFixture = (component: Type<T>): ComponentFixture<T> => {
        const fixture = TestBed.createComponent(component);
        return fixture;
    }
}

export default ComponentWrapper;
