import { CapitalizePipe } from '../../app/pipes/capitalize.pipe';
import { MockBackend } from '@angular/http/testing';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { BaseRequestOptions, Http, HttpModule } from '@angular/http';

import { AppComponent } from '../../app/app.component';
import { BoardContainerComponent } from '../../app/components/board-container/board-container.component';
import { BoardSectionComponent } from '../../app/components/board-section/board-section.component';
import { DeepBoxComponent } from '../../app/components/deep-box/deep-box.component';

import { BoardHeaderComponent } from '../../app/components/board-header/board-header.component';


import { BoardDataService } from '../../app/services/board-data.service';

export const DECLARATIONS = [
    AppComponent,
    BoardContainerComponent,
    BoardHeaderComponent,
    BoardSectionComponent,
    DeepBoxComponent,
    CapitalizePipe
];
export const IMPORTS = [
    BrowserModule,
    FormsModule,
    HttpModule
];

export const ALL_PRE_CONFIGURATION = {
    declarations: DECLARATIONS,
    imports: IMPORTS,
    providers: [
        BoardDataService,
        BaseRequestOptions,
        MockBackend,
        {
          provide: Http,
          useFactory: (backend, options) => new Http(backend, options),
          deps: [MockBackend, BaseRequestOptions]
        }
    ]
};

export default { ALL_PRE_CONFIGURATION, IMPORTS, DECLARATIONS };
