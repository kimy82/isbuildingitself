/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { CapitalizePipe } from '../../app/pipes/capitalize.pipe';
describe('Capitalize', () => {

  describe('CapitalizePipe', () => {
    let subject;
    let result;
    let pipe;

    beforeEach(() => {
      pipe = new CapitalizePipe();
    });

    afterEach(() => {
      expect(subject).toEqual(result);
    });

    describe('the method support', () => {

      it('should support string', () => {
        subject = pipe.supports('visualboard');
        result  = true;
      });

      it('should not support null', () => {
        subject = pipe.supports(null);
        result  = false;
      });

      it('should not support NaN', () => {
        subject = pipe.supports(NaN);
        result  = false;
      });

      it('should not support new Object()', () => {
        subject = pipe.supports(new Object());
        result  = false;
      });

      it('should not support function(){}', () => {
        subject = pipe.supports(function(){});
        result  = false;
      });

    });

    describe('the method transform', () => {
      it('should transform string to Capitalized versions', () => {
        subject = pipe.transform('visual board');
        result  = 'Visual board';
      });

      it('should transform all strings to Capitalized versions', () => {
        subject = pipe.transform('visual board', true);
        result  = 'Visual Board';
      });

      it('should capitalize just one letter', () => {
        subject = pipe.transform('c', true);
        result  = 'C';
      });
    });

    describe('the method capitalizeWord', () => {
      it('should capitalized a word', () => {
        subject = pipe.capitalizeWord('something');
        result  = 'Something';
      });

      it('should only capitalized first char', () => {
        subject = pipe.capitalizeWord('something something something');
        result  = 'Something something something';
      });
    });
  });
});
