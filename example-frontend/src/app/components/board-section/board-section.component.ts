import { AfterViewInit, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

declare const $: JQueryStatic;

@Component({
  selector: 'app-board-section',
  templateUrl: './board-section.component.html',
  styleUrls: ['./board-section.component.less']
})
export class BoardSectionComponent implements OnInit {

  public status: String;

  constructor() {
  }

  public ngOnInit(): void {
    this.status = "An Example of Angular 2";
  }
}
