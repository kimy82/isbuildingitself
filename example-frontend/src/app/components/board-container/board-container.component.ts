import { BoardDataService } from '../../services/board-data.service';
import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-board-container',
  templateUrl: './board-container.component.html',
  styleUrls: ['./board-container.component.less']
})
export class BoardContainerComponent implements OnInit {

  public result:string;

  constructor(private _boardDataService: BoardDataService) { }

  public ngOnInit() {
    this._fetchExamples();
  }

  private _fetchExamples(): void {
    this._boardDataService.fetchExamples().subscribe((backendData: any) => {
        this.result= this.result + JSON.stringify(backendData);
      });
  }

}
