import { Http } from '@angular/http';
import { BehaviorSubject, Observable } from 'rxjs/Rx';
import { Injectable } from '@angular/core';
import { Settings } from '../../settings';
import 'rxjs/Rx';

export class NodeNotFoundError extends Error { }

type PrefixType = 'r' | 'e';

@Injectable()
export class BoardDataService {
  public static readonly GET_ALL_DATA_URL = 'example/get';

  constructor(private _http: Http) {
    this.fetchExamples()
  }

  /**
   * Obtains the nodes and the edges from the server
   *
   * @private
   * 
   *
   * @memberOf BoardDataService
   */
  public fetchExamples<T>(): Observable<T> {
    return this._http.get(Settings.backendUrl + BoardDataService.GET_ALL_DATA_URL)
      .map(res => res.json());
  }
}

