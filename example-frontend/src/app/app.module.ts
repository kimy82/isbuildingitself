import { BoardDataService } from './services/board-data.service';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { AppComponent } from './app.component';
import { BoardContainerComponent } from './components/board-container/board-container.component';
import { BoardSectionComponent } from './components/board-section/board-section.component';
import { BoardHeaderComponent } from './components/board-header/board-header.component';
import { CapitalizePipe } from './pipes/capitalize.pipe';
import { DeepBoxComponent } from './components/deep-box/deep-box.component';


@NgModule({
  declarations: [
    CapitalizePipe,
    AppComponent,
    BoardHeaderComponent,
    BoardContainerComponent,
    BoardSectionComponent,
    DeepBoxComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule
  ],
  providers: [
    BoardDataService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {

  constructor() { }
}
