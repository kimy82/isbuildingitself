import { Pipe, PipeTransform } from '@angular/core';

// Check if the value is supported for the pipe
export function isString(txt): boolean {
  return typeof txt === 'string';
}

@Pipe({
  name: 'capitalize'
})
export class CapitalizePipe implements PipeTransform {

  private _regexp: RegExp = /([^\W_]+[^\s-]*) */g;

  constructor() {}

  supports(txt: string): boolean {
    return isString(txt);
  }

  /**
   * Transform a string to capitalized string calling: capitalizeWord()
   *
   * @param {string} value
   * @param {boolean} [allWords] 
   * @returns {*}
   *
   * @memberOf CapitalizePipe
   */
  transform(value: string, allWords?: boolean): string {
    return (!value) ? '' :
      (!allWords) ?
        this.capitalizeWord(value) :
        value.replace(this._regexp, this.capitalizeWord);
  }

  capitalizeWord(txt: string): string {
    return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
  }

}
