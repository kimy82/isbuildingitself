package com.atos.example.pojo;

import org.springframework.http.HttpStatus;

public class ExceptionResponse {
	private String	message;
	private String	status	= HttpStatus.INTERNAL_SERVER_ERROR.name();

	public ExceptionResponse() {
		// Needed when jackson takes place
	}

	public ExceptionResponse(String message) {
		this.message = message;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
}