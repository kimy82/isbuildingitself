package com.atos.example.controller;

import java.util.List;

import org.apache.commons.collections.IteratorUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.atos.example.model.Example;
import com.atos.example.service.ExampleService;

@RestController
@RequestMapping(value = "/example")
public class ExampleController {

	@Autowired
	private ExampleService exampleService;

	@RequestMapping(value = "/save", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE)
	public Example save(@RequestBody Example example) {
		return this.exampleService.saveOrUpdate(example);
	}
	
	@RequestMapping(value = "/get", method = RequestMethod.GET)
	public List<Example> get() {
		return IteratorUtils.toList(this.exampleService.getRepository().findAll().iterator());
	}
}
