package com.atos.example.repository;

import org.springframework.data.repository.CrudRepository;

import com.atos.example.model.Example;

public interface ExampleRepository extends CrudRepository<Example, Long> {
}
