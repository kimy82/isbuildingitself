package com.atos.example.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Service;

import com.atos.example.model.Example;
import com.atos.example.repository.ExampleRepository;

@Service
public class ExampleService implements AbstractCrudService<Example> {

	@Autowired
	private ExampleRepository exampleRepository;

	@Override
	public CrudRepository<Example, Long> getRepository() {
		return exampleRepository;
	}
}
