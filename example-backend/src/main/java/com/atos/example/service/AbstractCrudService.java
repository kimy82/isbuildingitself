package com.atos.example.service;

import org.springframework.data.repository.CrudRepository;

public interface AbstractCrudService<E> {
	public abstract CrudRepository<E, Long> getRepository();

	/**
	 * Will save or update (if id is <b>not null</b>) <br>
	 * <b>NOTICE: Will NOT do a flush </b>
	 * 
	 * @param entity Entity to be saved to the database
	 * @return
	 * @author a630641
	 */
	public default E saveOrUpdate(E entity) {
		return getRepository().save(entity);
	}

	/**
	 * Returns a list containing all items
	 * 
	 * @return
	 */
	public default Iterable<E> findAll() {
		return getRepository().findAll();
	}

	public default E findById(Long id) {
		return getRepository().findOne(id);
	}

	public default void delete(Long id) {
		getRepository().delete(id);
	}

}