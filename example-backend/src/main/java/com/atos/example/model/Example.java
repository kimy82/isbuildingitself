package com.atos.example.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "example")
public class Example {

	@Id
	@GeneratedValue
	private Long	id;

	@Column(nullable = false)
	private String	exampleText;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getExampleText() {
		return exampleText;
	}

	public void setExampleText(String exampleText) {
		this.exampleText = exampleText;
	}
}
