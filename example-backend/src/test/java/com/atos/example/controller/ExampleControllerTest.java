package com.atos.example.controller;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;

import org.junit.Test;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.atos.example.model.Example;
import com.atos.example.service.ExampleService;
import com.atos.example.test.ControllerEmbededDBRunner;
import com.atos.example.test.ControllerTestUtils;

@Import(value = { ExampleController.class, ExampleService.class, VisualDemandExceptionHandler.class })
public class ExampleControllerTest extends ControllerEmbededDBRunner {

	@Test
	public void requestIsSaved() throws Exception {
		Example exampleToSave = new Example();
		exampleToSave.setExampleText("exampleText");

		MvcResult resultOfRequest = mockMvc
				.perform(MockMvcRequestBuilders.post("/example/save").contentType(MediaType.APPLICATION_JSON)
						.content(ControllerTestUtils.asJsonString(exampleToSave)))
				.andExpect(MockMvcResultMatchers.status().isOk()).andReturn();

		Example savedExample = ControllerTestUtils
				.fromJsonStringToObject(resultOfRequest.getResponse().getContentAsString(), Example.class);
		assertThat(savedExample.getExampleText(), is(equalTo("exampleText")));
	}

}