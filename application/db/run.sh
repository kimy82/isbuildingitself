pgdata="$PWD/db";
initsql="$PWD/docker-entrypoint-initdb.d";
postgresContainerName="example-postgres"

#Stop PostgreSQL container if running
if docker inspect --format="{{ .State.StartedAt }}" $postgresContainerName &> /dev/null; then
    docker rm -f $postgresContainerName
fi

#run docker file
echo "Starting Postgres DB"

docker run -d --name $postgresContainerName -e "POSTGRES_PASSWORD=example" -e "POSTGRES_USER=example" -e "POSTGRES_DB=example" -e "PGDATA=$pgdata" -v $initsql:/docker-entrypoint-initdb.d -p 5432:5432 postgres:9.4