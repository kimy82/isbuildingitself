supervisorLogsTmp="$PWD/tmp";
builderTmpImageName="example/example-tmp";
builderTmpName="example-build-tmp";

#Stop containers
if docker inspect --format="{{ .State.StartedAt }}" example-build-tmp &> /dev/null; then
    docker rm -f example-build-tmp
fi

if docker inspect --format="{{ .State.StartedAt }}" example-backend &> /dev/null; then
    docker rm -f example-backend
fi

if docker inspect --format="{{ .State.StartedAt }}" example-nginx &> /dev/null; then
    docker rm -f example-nginx
fi

#Remove tmp directory
if [ "$(ls -A $supervisorLogsTmp)" ]; then
    rm -r  $supervisorLogsTmp
fi

#Build docker file
if [ -f Dockerfile ]; then
	docker build -t "$builderTmpImageName" .
fi

#run docker file
echo "Starting TMP container for Project setUp"
docker  run  -v /var/run/docker.sock:/var/run/docker.sock  -v "$supervisorLogsTmp":/tmp --name="$builderTmpName" "$builderTmpImageName"

