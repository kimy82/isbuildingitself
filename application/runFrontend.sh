while ! grep -q frontend_cloned=true "/processes.txt"; do
	echo "Waiting for Frontend to be cloned"
	sleep 3;
done
cd /frontend/isbuildingitself/example-frontend
npm install && ng build --prod 
echo 'angular_build=true' >> /processes.txt
