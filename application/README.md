## README

### What is for?
Downloads frontend and backend projects for Example. It builds frontend and backend and creates a nginx docker container that holds the application. (nginx with Angular2 frontend and springBoot Backend )

### How to use?
Run _./run.sh_
* It will create a temporary docker container where it will run all programs in supervisord.conf.
* While running it will create a tmp folder where you can track the progress of which task.
* If the program endsw with 0 it means that the program has terminated successfuly. Any other exit status is wrong.
