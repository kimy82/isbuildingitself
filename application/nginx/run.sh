localImageName="example/nginx";
containerName="example-nginx";

docker rm -f $containerName;

#Check arguments. If none passed 8080 is the default nginx port
if [ -z "$1" ]; then
	port="8080"
else
	port=$1
fi

#Check arguments. If one passed backend ip is a docker windows ip '192.168.99.100'
if [ -z "$2" ]; then
	backend_ip="192.168.99.100"
else
	backend_ip=$2
fi

#Wait till angular code has been build.
while ! grep -q angular_build=true "/processes.txt"; do
	echo "Waiting for Angular project build"
	sleep 3;
done

echo "Building Nginx container"
docker cp example-build-tmp:/frontend/isbuildingitself/example-frontend/dist ./static-html-directory
docker build -t "$localImageName" .
docker run -d --name $containerName -p $port:80 --add-host backend_host:$backend_ip  "$localImageName"
status=$?
echo 'nginx_running=true' >> /processes.txt
exit $status
					
