while ! grep -q backend_cloned=true "/processes.txt"; do
	echo "Waiting for Backend to be cloned"
	sleep 3;
done

cd /backend/isbuildingitself/example-backend
mvn clean package docker:build -Dmaven.test.skip=true
docker run -d -p 2030:2030 --name=example-backend  example/example-backend

