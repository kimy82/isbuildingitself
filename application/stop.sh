builderTmpName="example-build-tmp";
nginxConatinername="example-nginx";

#Wait till nginx container has started. Then destroy the tmp container.
while ! docker inspect --format="{{ .State.StartedAt }}" $nginxConatinername &> /dev/null
do
	sleep 3s
    echo "Nginx still not started"
done

docker rm -f $builderTmpName;

